package com.lhvtestassignment.blacklist;

import com.lhvtestassignment.blacklist.data.NameItem;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
public class MatchItem {

  private final InputItem inputItem;
  private final NameItem nameItem;

  public List<String> getCommonPhoneticCodes() {
    List<String> inputPhoneticCodes = inputItem.getInputPhoneticCodes();
    return nameItem.getPhoneticCodes().stream()
        .distinct()
        .filter(inputPhoneticCodes::contains)
        .collect(toList());
  }

  public Map<String, String> getNamePhoneticCodesByNamePart() {
    return nameItem.getPhoneticCodesByNamePart();
  }

  public Map<String, List<String>> getInputPartsByPhoneticCode() {
    return inputItem.getInputPartsByPhoneticCode();
  }

  public String getInput() {
    return inputItem.getInput();
  }

  public String getName() {
    return nameItem.getFullName();
  }

  public String getFirstName() {
    return nameItem.getFirstName();
  }

  public String getLastName() {
    return nameItem.getLastName();
  }

}
