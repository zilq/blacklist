package com.lhvtestassignment.blacklist.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class FileUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

  public static List<String> getFileLines(String fileName) {
    File file = getFile(fileName);
    if (file == null || !file.exists() || !file.isFile()) {
      LOGGER.error(String.format("%s is not a valid file!", fileName));
      return emptyList();
    }

    try (BufferedReader reader = new BufferedReader(new FileReader(file))){
      return reader.lines().collect(toList());
    } catch (IOException ex) {
      LOGGER.error(ex.getMessage());
    }

    return emptyList();
  }

  private static File getFile(String fileName) {
    try {
      return new ClassPathResource(fileName).getFile();
    } catch (IOException ex) {
      LOGGER.error(ex.getMessage());
    }

    return null;
  }
}
