package com.lhvtestassignment.blacklist.filter;

import com.lhvtestassignment.blacklist.MatchItem;
import com.lhvtestassignment.blacklist.rating.MatchRating;

import java.util.Map;
import java.util.function.Predicate;

public class RequiredNamePartsFilter implements Predicate<Map.Entry<MatchItem, MatchRating>> {

  @Override
  public boolean test(Map.Entry<MatchItem, MatchRating> entry) {
    MatchItem item = entry.getKey();
    MatchRating rating = entry.getValue();
    return rating.hasRatingForNamePart(item.getFirstName())
        && rating.hasRatingForNamePart(item.getLastName());
  }

}
