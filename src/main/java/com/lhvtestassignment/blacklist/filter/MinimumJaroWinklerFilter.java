package com.lhvtestassignment.blacklist.filter;

import com.lhvtestassignment.blacklist.MatchItem;
import com.lhvtestassignment.blacklist.rating.MatchRating;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class MinimumJaroWinklerFilter implements Predicate<Map.Entry<MatchItem, MatchRating>> {

  private final Double minimumAllowedJaroWinklerValue;

  @Override
  public boolean test(Map.Entry<MatchItem, MatchRating> entry) {
    MatchItem item = entry.getKey();
    MatchRating rating = entry.getValue();
    String firstName = item.getFirstName();
    String lastName = item.getLastName();

    boolean hasRatings = rating.hasRatingForNamePart(firstName) && rating.hasRatingForNamePart(lastName);

    return hasRatings
        && isValidJaroWinklerValue(rating.getRating(firstName))
        && isValidJaroWinklerValue(rating.getRating(lastName));
  }

  private boolean isValidJaroWinklerValue(Double value) {
    return minimumAllowedJaroWinklerValue.compareTo(value) <= 0;
  }

}
