package com.lhvtestassignment.blacklist;

import com.lhvtestassignment.blacklist.util.NameUtil;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Builder
@RequiredArgsConstructor
public class InputItem {

  private final String input;
  private final List<String> inputParts;
  private final Map<String, List<String>> inputPartsByPhoneticCode;

  public InputItem(String input, List<String> inputParts) {
    this.input = input;
    this.inputParts = inputParts;
    this.inputPartsByPhoneticCode = createInputPartsByPhoneticCode();
  }

  private Map<String, List<String>> createInputPartsByPhoneticCode() {
    Map<String, List<String>> inputByPhoneticCodes = new HashMap<>();
    inputParts.forEach(item -> {
      String phoneticCode = NameUtil.getPhoneticCode(item);
      List<String> items = inputByPhoneticCodes.getOrDefault(phoneticCode, new ArrayList<>());
      items.add(item);
      inputByPhoneticCodes.put(phoneticCode, items);
    });

    return inputByPhoneticCodes;
  }

  public List<String> getInputPhoneticCodes() {
    return List.copyOf(inputPartsByPhoneticCode.keySet());
  }

  public Map<String, List<String>> getInputPartsByPhoneticCode() {
    return inputPartsByPhoneticCode;
  }

  public String getInput() {
    return input;
  }

}
