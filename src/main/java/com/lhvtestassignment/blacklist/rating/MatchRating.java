package com.lhvtestassignment.blacklist.rating;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;

public class MatchRating {

  private final Map<String, Pair<String, Double>> inputPartScoresByNamePart = new HashMap<>();

  public void addRating(String namePart, Pair<String, Double> inputPartRating) {
    inputPartScoresByNamePart.put(namePart.toLowerCase(), inputPartRating);
  }

  public boolean hasRatingForNamePart(String namePart) {
    return inputPartScoresByNamePart.containsKey(namePart.toLowerCase());
  }

  public Double getRating(String namePart) {
    if (hasRatingForNamePart(namePart)) {
      return inputPartScoresByNamePart.get(namePart.toLowerCase()).getRight();
    }

    return null;
  }

}
