package com.lhvtestassignment.blacklist;

import com.lhvtestassignment.blacklist.rating.MatchResult;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.Scanner;

@Profile("!test")
@SpringBootApplication
@RequiredArgsConstructor
public class BlacklistApplication implements CommandLineRunner {

	private final static String EXIT = "EXIT";
	private final BlackListMatcher matcher;

	public static void main(String[] args) {
		SpringApplication.run(BlacklistApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		Scanner scanner = new Scanner(System.in);
		System.out.println("\nEnter a name to find a match from the blacklist. Enter 'exit' to exit application.");
		System.out.print("> ");

		String input = scanner.nextLine();
		while (!EXIT.equalsIgnoreCase(input)) {
			List<MatchResult> results = matcher.findMatch(input);
			results.forEach(System.out::println);
			System.out.print("> ");
			input = scanner.nextLine();
		}
	}
}
