package com.lhvtestassignment.blacklist.filter;


import com.lhvtestassignment.blacklist.MatchItem;
import com.lhvtestassignment.blacklist.rating.MatchRating;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.lhvtestassignment.blacklist.util.TestUtil.createEntry;
import static com.lhvtestassignment.blacklist.util.TestUtil.createMatchItem;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MinimumJaroWinklerFilterTest {

  private static final Double MINIMUM_ALLOWED_RATING = 0.89;
  private final MinimumJaroWinklerFilter jaroWinklerFilter = new MinimumJaroWinklerFilter(MINIMUM_ALLOWED_RATING);

  @Test
  public void validEntryPassesFilter() {
    assertTrue(jaroWinklerFilter.test(createEntry(MINIMUM_ALLOWED_RATING)));
  }

  @Test
  public void entryWithTooLowRatingsDoesNotPassFilter() {
    assertFalse(jaroWinklerFilter.test(createEntry(MINIMUM_ALLOWED_RATING / 2)));
  }

  @Test
  public void entryWithoutRequiredNamePartRatingsDoesNotPassFilter() {
    MatchItem matchItem = createMatchItem();
    MatchRating matchRating = new MatchRating();
    assertFalse(jaroWinklerFilter.test(Map.entry(matchItem, matchRating)));
  }

}