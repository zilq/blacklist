package com.lhvtestassignment.blacklist.filter;

import com.lhvtestassignment.blacklist.MatchItem;
import com.lhvtestassignment.blacklist.rating.MatchRating;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.lhvtestassignment.blacklist.util.TestUtil.createEntry;
import static com.lhvtestassignment.blacklist.util.TestUtil.createMatchItem;
import static org.junit.jupiter.api.Assertions.*;

class RequiredNamePartsFilterTest {

  private final RequiredNamePartsFilter namePartsFilter = new RequiredNamePartsFilter();

  @Test
  public void validEntryPassesFilter() {
    assertTrue(namePartsFilter.test(createEntry(0.89)));
  }

  @Test
  public void entryWithNoRatingsForRequiredNamePartsDoesNotPassFilter() {
    MatchItem matchItem = createMatchItem();
    MatchRating matchRating = new MatchRating();
    assertFalse(namePartsFilter.test(Map.entry(matchItem, matchRating)));
  }
}