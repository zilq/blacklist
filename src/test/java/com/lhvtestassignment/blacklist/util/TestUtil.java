package com.lhvtestassignment.blacklist.util;

import com.lhvtestassignment.blacklist.InputItem;
import com.lhvtestassignment.blacklist.MatchItem;
import com.lhvtestassignment.blacklist.data.NameItem;
import com.lhvtestassignment.blacklist.rating.MatchRating;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class TestUtil {

  private static final String INPUT = "Lugupeetud Artur Aabits";

  public static String getInput() {
    return INPUT;
  }

  public static MatchItem createMatchItem() {
    return new MatchItem(createInputItem(), createNameItem());
  }

  public static Map.Entry<MatchItem, MatchRating> createEntry(Double jaroWinklerRating) {
    MatchItem matchItem = new MatchItem(createInputItem(), createNameItem());
    MatchRating matchRating = createMatchRating(jaroWinklerRating);

    return Map.entry(matchItem, matchRating);
  }

  private static NameItem createNameItem() {
    return NameItem.builder()
        .firstName("Artur")
        .middleNames(emptyList())
        .lastName("Aabits")
        .fullName("Artur Aabits")
        .phoneticCodesByNamePart(Map.of("Artur", "ARTR", "Aabits", "ABTS"))
        .build();
  }

  private static InputItem createInputItem() {
    return InputItem.builder()
        .input(INPUT)
        .inputParts(List.of("lugupeetud", "artur", "aabits"))
        .inputPartsByPhoneticCode(createInputPartsByPhoneticCode())
        .build();
  }

  private static Map<String, List<String>> createInputPartsByPhoneticCode() {
    return Map.of(
        "LGPTD", singletonList("lugupeetud"),
        "ARTR", singletonList("artur"),
        "ABTS", singletonList("aabits")
    );
  }

  private static MatchRating createMatchRating(Double jaroWinklerRating) {
    MatchRating matchRating = new MatchRating();
    matchRating.addRating("Artur", Pair.of("artur", jaroWinklerRating));
    matchRating.addRating("Aabits", Pair.of("aabits", jaroWinklerRating));

    return matchRating;
  }

}
