package com.lhvtestassignment.blacklist.rating;

import com.lhvtestassignment.blacklist.MatchItem;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.JaroWinklerSimilarity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.lhvtestassignment.blacklist.util.TestUtil.createMatchItem;
import static com.lhvtestassignment.blacklist.util.TestUtil.getInput;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MatchRatingProviderTest {

  private static final int RESULT_LIST_SIZE = 1;
  private static final Double MINIMUM_JARO_WINKLER_VALUE = 0.89;
  private static JaroWinklerSimilarity jaroWinklerSimilarity;
  private static MatchRatingProvider ratingProvider;

  @BeforeAll
  public static void setUp() {
    jaroWinklerSimilarity = mock(JaroWinklerSimilarity.class);
    ratingProvider = new MatchRatingProvider(MINIMUM_JARO_WINKLER_VALUE, RESULT_LIST_SIZE, jaroWinklerSimilarity);
  }

  @Test
  public void validMatchItemProducesValidResult() {
    when(jaroWinklerSimilarity.apply(anyString(), anyString())).thenReturn(MINIMUM_JARO_WINKLER_VALUE);
    List<MatchItem> matchItems = singletonList(createMatchItem());
    List<MatchResult> results = ratingProvider.findBestMatches(matchItems, getInput());
    assertTrue(results.size() == 1 && results.get(0).getMatchedName().equals("Artur Aabits"));
  }

  @Test
  public void tooLowJaroWinklerRatingsReturnEmptyResult() {
    when(jaroWinklerSimilarity.apply(anyString(), anyString())).thenReturn(MINIMUM_JARO_WINKLER_VALUE / 2);
    List<MatchItem> matchItems = singletonList(createMatchItem());
    List<MatchResult> results = ratingProvider.findBestMatches(matchItems, getInput());
    assertTrue(results.size() == 1 && StringUtils.isBlank(results.get(0).getMatchedName()));
  }

}