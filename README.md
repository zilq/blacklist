# Blacklist input matcher
A simple implementation of a double bypass string matching strategy
against a blacklist file. The best matches are found using a combination 
of phonetic matching and Jaro-Winkler similarity ratings.

### Matching strategy
User input is mapped to its phonetic codes based on the output 
from a double metaphone algorithm. The first bypass matches names from 
the blacklist which contain at least one phonetic code found in the 
given input. The second bypass compares parts of a name (i.e. first and 
last name) to parts of the input that have the same phonetic properties.
The input parts are given a score based on how similar they are to their 
blacklist phonetic counterpart. Names for which their phonetically 
corresponding input parts do not meet the minimum allowed Jaro-Winkler 
rating are filtered out.

Both phonetic matching and Jaro-Winkler similarity ratings take possible 
typos into account. During phonetic matching phonetically similar 
names are found as long as the typo does not alter the phonetic code 
for the input part being processed.

The extent that the Jaro-Winkler similarity rating is altered due to 
a misspelling depends on the typo itself. The rating takes less of a hit 
if the typo occurs in the middle of the word and involves adjacent 
characters, and the word itself is not too short compared to the number
of characters mistyped. 

### Configuration
The project allows limited configuration. The configuration properties 
have to be placed into `src/main/resources/application.properties`.
Properties that can be configured:
- Blacklist and noise word filenames (files have to be in `src/main/resources/`)
- The minimum allowed Jaro-Winkler rating
- The maximum result list size

### Running the application
`./gradlew bootRun` inside a terminal window or through an IDE after 
importing the gradle project.